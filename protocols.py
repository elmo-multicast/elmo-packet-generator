from scapy.fields import XBitField
from scapy.layers.inet import Ether, IP, UDP
from scapy.layers.vxlan import VXLAN
from scapy.packet import Packet, bind_layers, bind_bottom_up, bind_top_down


class Elmo(Packet):
    name = "Elmo"
    fields_desc = [XBitField("direction", 0, size=1)]
    has_core = False

    @classmethod
    def reset(cls):
        cls.name = "Elmo"
        cls.fields_desc = [XBitField("direction", 0, size=1)]
        cls.has_core = False

    @classmethod
    def add_padding(cls):
        size = sum([f.size for f in cls.fields_desc])
        if not (size % 8 == 0):
            cls.fields_desc += [XBitField("padding", 0, size=(8 - (size % 8)))]

    @classmethod
    def add_upstream_prule(cls, name="leaf", downstream_bitmap_size=8, upstream_bitmap_size=8):
        cls.fields_desc += [
            XBitField("upstream_" + name + "_downstreamBitmap", 0, size=downstream_bitmap_size),
            XBitField("upstream_" + name + "_upstreamBitmap", 0, size=upstream_bitmap_size),
            XBitField("upstream_" + name + "_multipath", 0, size=1)]

    @classmethod
    def add_core_prule(cls, bitmap_size=8):
        if not cls.has_core:
            cls.fields_desc += [
                XBitField("core_bitmap", 0, size=bitmap_size)]
            cls.has_core = True

    @classmethod
    def add_downstream_prules(cls, name="leaf", num_prules=2, bitmap_size=8,
                              num_nodes_per_prule={1: 2, 2: 2}, node_id_size=4,
                              has_default_bitmap=False):

        for prule_idx in range(num_prules):
            cls.fields_desc += [
                XBitField("downstream_" + name + "_prule" + str(prule_idx) + "DownstreamBitmap", 0,
                          size=bitmap_size)]
            cls.fields_desc += [XBitField("downstream_" + name + "_prule" + str(prule_idx) + "HasPRule", 0,
                                          size=1)]
            if prule_idx == 0:
                cls.fields_desc += [XBitField("downstream_" + name + "_prule" + str(prule_idx) + "HasDefaultBitmap", 0,
                                              size=1)]

            node_name = 'Switch' if name == 'leaf' else 'Pod'
            for node_idx in range(num_nodes_per_prule[prule_idx + 1]):
                cls.fields_desc += [
                    XBitField("downstream_" + name + "_prule" + str(prule_idx) + node_name + str(node_idx), 0,
                              size=node_id_size)]
                cls.fields_desc += [XBitField("downstream_" + name + "_prule" + str(prule_idx) +
                                              node_name + str(node_idx) + "Next", 0, size=1)]

        if has_default_bitmap:
            cls.fields_desc += [XBitField("downstream_" + name + "_defaultBitmap", 0, size=bitmap_size)]


# Elmo Core Packet
elmo_core_packet_config = {
        "core_prule": {
            "bitmap_size": 8
        }
    }


class ElmoCorePacket(Elmo):
    name = "ElmoCorePacket"

    @classmethod
    def init(cls, config):
        cls.reset()

        if "core_prule" in config:
            cls.add_core_prule(bitmap_size=config["core_prule"]["bitmap_size"])
        cls.add_padding()


# Dissection
bind_bottom_up(VXLAN, ElmoCorePacket, NextProtocol=10)
bind_layers(ElmoCorePacket, Ether)
# Build
bind_top_down(VXLAN, ElmoCorePacket, flags=12, NextProtocol=10)

ElmoCorePacket.init(elmo_core_packet_config)


# Elmo LeafCore Packet
elmo_leafcore_packet_config = {
        "upstream_leaf_prule": {
            "downstream_bitmap_size": 48,
            "upstream_bitmap_size": 16
        },
        "core_prule": {
            "bitmap_size": 8
        },
        "downstream_leaf_prules": {
            "num_prules": 20,
            "bitmap_size": 48,
            "num_nodes_per_prule": {i + 1: 5 for i in range(20)},
            "node_id_size": 8,
            "has_default_bitmap": True
        }
    }


class ElmoLeafCorePacket(Elmo):
    name = "ElmoLeafCorePacket"

    @classmethod
    def init(cls, config):
        cls.reset()

        if "upstream_leaf_prule" in config:
            cls.add_upstream_prule(name="leaf",
                                   downstream_bitmap_size=config["upstream_leaf_prule"]["downstream_bitmap_size"],
                                   upstream_bitmap_size=config["upstream_leaf_prule"]["upstream_bitmap_size"])
        if "core_prule" in config:
            cls.add_core_prule(bitmap_size=config["core_prule"]["bitmap_size"])
        if "downstream_leaf_prules" in config:
            cls.add_downstream_prules(name="leaf",
                                      num_prules=config["downstream_leaf_prules"]["num_prules"],
                                      bitmap_size=config["downstream_leaf_prules"]["bitmap_size"],
                                      num_nodes_per_prule=config["downstream_leaf_prules"]["num_nodes_per_prule"],
                                      node_id_size=config["downstream_leaf_prules"]["node_id_size"],
                                      has_default_bitmap=config["downstream_leaf_prules"]["has_default_bitmap"])
        cls.add_padding()


# Dissection
bind_bottom_up(VXLAN, ElmoLeafCorePacket, NextProtocol=11)
bind_layers(ElmoLeafCorePacket, Ether)
# Build
bind_top_down(VXLAN, ElmoLeafCorePacket, flags=12, NextProtocol=11)

ElmoLeafCorePacket.init(elmo_leafcore_packet_config)


# Elmo LeafSpineCore Packet
elmo_leafspinecore_packet_config = {
        "upstream_leaf_prule": {
            "downstream_bitmap_size": 48,
            "upstream_bitmap_size": 16
        },
        "upstream_spine_prule": {
            "downstream_bitmap_size": 16,
            "upstream_bitmap_size": 8
        },
        "core_prule": {
            "bitmap_size": 8
        },
        "downstream_spine_prules": {
            "num_prules": 5,
            "bitmap_size": 16,
            "num_nodes_per_prule": {i + 1: 2 for i in range(5)},
            "node_id_size": 8,
            "has_default_bitmap": True
        },
        "downstream_leaf_prules": {
            "num_prules": 20,
            "bitmap_size": 48,
            "num_nodes_per_prule": {i + 1: 5 for i in range(20)},
            "node_id_size": 8,
            "has_default_bitmap": True
        }
    }


class ElmoLeafSpineCorePacket(Elmo):
    name = "ElmoLeafSpineCorePacket"

    @classmethod
    def init(cls, config):
        cls.reset()

        if "upstream_leaf_prule" in config:
            cls.add_upstream_prule(name="leaf",
                                   downstream_bitmap_size=config["upstream_leaf_prule"]["downstream_bitmap_size"],
                                   upstream_bitmap_size=config["upstream_leaf_prule"]["upstream_bitmap_size"])
        if "upstream_spine_prule" in config:
            cls.add_upstream_prule(name="spine",
                                   downstream_bitmap_size=config["upstream_spine_prule"]["downstream_bitmap_size"],
                                   upstream_bitmap_size=config["upstream_spine_prule"]["upstream_bitmap_size"])
        if "core_prule" in config:
            cls.add_core_prule(bitmap_size=config["core_prule"]["bitmap_size"])
        if "downstream_spine_prules" in config:
            cls.add_downstream_prules(name="spine",
                                      num_prules=config["downstream_spine_prules"]["num_prules"],
                                      bitmap_size=config["downstream_spine_prules"]["bitmap_size"],
                                      num_nodes_per_prule=config["downstream_spine_prules"][
                                          "num_nodes_per_prule"],
                                      node_id_size=config["downstream_spine_prules"]["node_id_size"],
                                      has_default_bitmap=config["downstream_spine_prules"]["has_default_bitmap"])
        if "downstream_leaf_prules" in config:
            cls.add_downstream_prules(name="leaf",
                                      num_prules=config["downstream_leaf_prules"]["num_prules"],
                                      bitmap_size=config["downstream_leaf_prules"]["bitmap_size"],
                                      num_nodes_per_prule=config["downstream_leaf_prules"][
                                          "num_nodes_per_prule"],
                                      node_id_size=config["downstream_leaf_prules"]["node_id_size"],
                                      has_default_bitmap=config["downstream_leaf_prules"]["has_default_bitmap"])
        cls.add_padding()


# Dissection
bind_bottom_up(VXLAN, ElmoLeafSpineCorePacket, NextProtocol=12)
bind_layers(ElmoLeafSpineCorePacket, Ether)
# Build
bind_top_down(VXLAN, ElmoLeafSpineCorePacket, flags=12, NextProtocol=12)

ElmoLeafSpineCorePacket.init(elmo_leafspinecore_packet_config)


if __name__ == "__main__":
    core_pkt = Ether() / IP() / UDP() / VXLAN() / ElmoCorePacket(direction=1, core_bitmap=0xee) / Ether() / IP()
    core_pkt.show()
    core_pkt.show2()

    leafcore_pkt = Ether() / IP() / UDP() / VXLAN() / ElmoLeafCorePacket(direction=1, core_bitmap=0xee) / Ether() / IP()
    leafcore_pkt.show()
    leafcore_pkt.show2()

    leafspinecore_pkt = Ether() / IP() / UDP() / VXLAN() / ElmoLeafSpineCorePacket(direction=1, core_bitmap=0xee) / \
                        Ether() / IP()
    leafspinecore_pkt.show()
    leafspinecore_pkt.show2()

