from scapy.layers.inet import Ether, IP, UDP
from scapy.layers.vxlan import VXLAN
from scapy.sendrecv import sendp

from protocols import ElmoLeafSpineCorePacket
from utils import BM

# Creating a custom leaf,spine,core packet format. Figure 3a, Redundancy (R) = 2.
elmo_leafspinecore_packet_config = {
        "upstream_leaf_prule": {
            "downstream_bitmap_size": 2,
            "upstream_bitmap_size": 2
        },
        "upstream_spine_prule": {
            "downstream_bitmap_size": 2,
            "upstream_bitmap_size": 2
        },
        "core_prule": {
            "bitmap_size": 4
        },
        "downstream_spine_prules": {
            "num_prules": 2,
            "bitmap_size": 2,
            "num_nodes_per_prule": {1: 1, 2: 2},
            "node_id_size": 2,  # in bits
            "has_default_bitmap": False
        },
        "downstream_leaf_prules": {
            "num_prules": 2,
            "bitmap_size": 2,
            "num_nodes_per_prule": {1: 2, 2: 2},
            "node_id_size": 3,  # in bits
            "has_default_bitmap": False
        }
    }

ElmoLeafSpineCorePacket.init(config=elmo_leafspinecore_packet_config)

# Packet originating from Host H_a in Figure 3a.
elmo_pkt = ElmoLeafSpineCorePacket(
    # type ###################################################
    direction=1,
    # upstream leaf p-rule ###################################
    upstream_leaf_downstreamBitmap=BM('01'),
    upstream_leaf_upstreamBitmap=BM('00'),
    upstream_leaf_multipath=1,
    # upstream spine p-rule ##################################
    upstream_spine_downstreamBitmap=BM('00'),
    upstream_spine_upstreamBitmap=BM('00'),
    upstream_spine_multipath=1,
    # core p-rule ############################################
    core_bitmap=BM('0011'),
    # downstream spine p-rules ###############################
    # p-rule 0
    downstream_spine_prule0DownstreamBitmap=BM('10'),
    downstream_spine_prule0HasPRule=1,
    downstream_spine_prule0HasDefaultBitmap=0,
    downstream_spine_prule0Pod0=0,
    downstream_spine_prule0Pod0Next=0,
    # p-rule 1
    downstream_spine_prule1DownstreamBitmap=BM('11'),
    downstream_spine_prule1HasPRule=0,
    downstream_spine_prule1Pod0=2,
    downstream_spine_prule1Pod0Next=1,
    downstream_spine_prule1Pod1=3,
    downstream_spine_prule1Pod1Next=0,
    # downstream leaf p-rules #################################
    # p-rule 0
    downstream_leaf_prule0DownstreamBitmap=BM('11'),
    downstream_leaf_prule0HasPRule=1,
    downstream_leaf_prule0HasDefaultBitmap=0,
    downstream_leaf_prule0Switch0=0,
    downstream_leaf_prule0Switch0Next=1,
    downstream_leaf_prule0Switch1=6,
    downstream_leaf_prule0Switch1Next=0,
    # p-rule 1
    downstream_leaf_prule1DownstreamBitmap=BM('11'),
    downstream_leaf_prule1HasPRule=0,
    downstream_leaf_prule1Switch0=5,
    downstream_leaf_prule1Switch0Next=1,
    downstream_leaf_prule1Switch1=7,
    downstream_leaf_prule1Switch1Next=0)

# elmo_pkt.show()
elmo_pkt.show2()

elmo_pkt_bytes = bytes(elmo_pkt)
print("Bytes: " + str(list(elmo_pkt_bytes)))
print("No. of Bytes: " + str(len(elmo_pkt_bytes)))
print

pkt = Ether() / IP() / UDP() / VXLAN(vni=0xdeed) / elmo_pkt / Ether() / IP()

# pkt.show()
pkt.show2()

# pkt_bytes = bytes(pkt)
# print(list(pkt_bytes))
# print(len(pkt_bytes))

sendp(pkt, iface='en0')
